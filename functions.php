<?php

require_once get_stylesheet_directory() . '/inc/custom-metaboxes.php';
require_once get_stylesheet_directory() . '/inc/ajax.php';
require_once get_stylesheet_directory() . '/inc/admin.php';
require_once get_stylesheet_directory() . '/inc/customizer.php';
require_once get_stylesheet_directory() . '/inc/Walkernav.php';

function register_voxel_session(){
  if( !session_id() ){
    session_start();
  }
}
add_action('init', 'register_voxel_session', 1);

function theme_setup() {
	load_theme_textdomain( 'adventurebeta_theme_dark' );

	// Adding menus to wp controll pannel
	register_nav_menus(array(
		'main_menu_one' => __('Main menu'),
		'mobile_menu' => __('Mobile menu')
	)); 

	//post thum pictures
    add_theme_support('post-thumbnails');

	add_image_size('game-thumbnail', 210, 210, array('center', 'center'));
	add_image_size('game-thumbnail-2x', 340, 340, array('center', 'center'));
    add_image_size('game-thumbnail-3x', 550, 550, array('center', 'center'));  
    add_image_size('game-thumbnail-4x', 650, 650, array('center', 'center'));  

    add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'theme_setup' );

//add theme support end

//adding css styles start
function add_styles_js(){
	wp_enqueue_style('style', get_theme_file_uri( '/css/main.min.css'),  array(), '1.0.5');
	wp_enqueue_script('scroll_rev', get_template_directory_uri() . '/js/scrollreviel.min.js', array(), 1.0, false);
    wp_enqueue_script('scrollbar', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array(), 1.0, true);
	wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array('jquery'), 1.3, true);

    if(is_singular('games')){
       wp_localize_script('main_js', 'postID', array( 'id' => get_the_ID() )); 
       wp_enqueue_script('adblock_checker', get_stylesheet_directory_uri() . '/js/ads.js', array(), 1.0, false); 
    }
}
add_action('wp_enqueue_scripts', 'add_styles_js');
//adding css styles end

function set_excerpt_length(){
    if(has_post_thumbnail()){
        return 12;  
    }else{
        return 25;
    }  	
}
add_filter('excerpt_length', 'set_excerpt_length');

//cpt start
function custom_post_type() {
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Game', 'Post Type General Name', 'adventurebeta_theme_dark' ),
        'singular_name'       => _x( 'Game', 'Post Type Singular Name', 'adventurebeta_theme_dark' ),
        'menu_name'           => __( 'Games', 'adventurebeta_theme_dark' ),
        'parent_item_colon'   => __( 'Parent Games', 'adventurebeta_theme_dark' ),
        'all_items'           => __( 'All Games', 'adventurebeta_theme_dark' ),
        'view_item'           => __( 'View Games', 'adventurebeta_theme_dark' ),
        'add_new_item'        => __( 'Add New Games', 'adventurebeta_theme_dark' ),
        'add_new'             => __( 'Add New', 'adventurebeta_theme_dark' ),
        'edit_item'           => __( 'Edit Game', 'adventurebeta_theme_dark' ),
        'update_item'         => __( 'Update Games', 'adventurebeta_theme_dark' ),
        'search_items'        => __( 'Search Games', 'adventurebeta_theme_dark' ),
        'not_found'           => __( 'Not Found', 'adventurebeta_theme_dark' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'adventurebeta_theme_dark' ),
    );
     
// Set other options for Custom Post Type	     
    $args = array(
        'label'               => __( 'games', 'adventurebeta_theme_dark' ),
        'description'         => __( 'Games', 'adventurebeta_theme_dark' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 10,
        'can_export'          => true,  
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'menu_icon'           => 'dashicons-images-alt',
        'taxonomies'          => array( 'game_types' ),
        'has_archive'         => true,
    );

    // Registering your Custom Post Type
    register_post_type( 'games', $args );
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
add_action( 'init', 'custom_post_type', 0 );

// create two taxonomies, genres and writers for the post type "book"
function game_types_tax() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Game types', 'taxonomy general name', 'adventurebeta_theme_dark' ),
        'singular_name'     => _x( 'Game type', 'taxonomy singular name', 'adventurebeta_theme_dark' ),
        'search_items'      => __( 'Search For Game type', 'adventurebeta_theme_dark' ),
        'all_items'         => __( 'All Game types', 'adventurebeta_theme_dark' ),
        'parent_item'       => __( 'Parent type', 'adventurebeta_theme_dark' ),
        'parent_item_colon' => __( 'Parent type:', 'adventurebeta_theme_dark' ),
        'edit_item'         => __( 'Edit type', 'adventurebeta_theme_dark' ),
        'update_item'       => __( 'Update type', 'adventurebeta_theme_dark' ),
        'add_new_item'      => __( 'Add New type', 'adventurebeta_theme_dark' ),
        'new_item_name'     => __( 'New type Name', 'adventurebeta_theme_dark' ),
        'menu_name'         => __( 'Game types', 'adventurebeta_theme_dark' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
    );

    register_taxonomy( 'game_types', 'games', $args );

    $tag_labels = array(
        'name'                      => _x( 'Tags', 'taxonomy general name', 'adventurebeta_theme_dark' ),
        'singular_name'             => _x( 'Tag', 'taxonomy singular name', 'adventurebeta_theme_dark' ),
        'search_items'              =>  __( 'Search Tags', 'adventurebeta_theme_dark' ),
        'popular_items'             => __( 'Popular Tags', 'adventurebeta_theme_dark' ),
        'all_items'                 => __( 'All Tags', 'adventurebeta_theme_dark' ),
        'parent_item'               => null,
        'parent_item_colon'         => null,
        'edit_item'                 => __( 'Edit Tag', 'adventurebeta_theme_dark' ), 
        'update_item'               => __( 'Update Tag', 'adventurebeta_theme_dark' ),
        'add_new_item'              => __( 'Add New Tag', 'adventurebeta_theme_dark' ),
        'new_item_name'             => __( 'New Tag Name', 'adventurebeta_theme_dark' ),
        'separate_items_with_commas' => __( 'Separate tags with commas', 'adventurebeta_theme_dark' ),
        'add_or_remove_items'       => __( 'Add or remove tags', 'adventurebeta_theme_dark' ),
        'choose_from_most_used'     => __( 'Choose from the most used tags', 'adventurebeta_theme_dark' ),
        'menu_name'                 => __( 'Tags', 'adventurebeta_theme_dark' ),
    ); 
    
    $tag_args= array(
        'hierarchical'          => false,
        'labels'                => $tag_labels,
        'show_ui'               => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'tag' ),
    );

    register_taxonomy( 'game_tags', 'games', $tag_args );
}
add_action( 'init', 'game_types_tax', 0 );
// register custom taxonomy for clients post type end 

//cpt end

function custom_excerpt() {
    return '... ';
}
add_filter( 'excerpt_more', 'custom_excerpt' );

// adding class names to previous and next pagination links of get_next/previous_posts_link start

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return (current_filter() == 'next_posts_link_attributes') ? 'class="btn btn-primary next_post_link"' : 'class="btn btn-primary previous_post_link"';
}
// adding class names to previous and next pagination links of get_next/previous_posts_link end

// add hew bage to games start
function voxel_new_badge($post_time_stamp, $cur_time, $post_id){
    $php_date = date("Y/m/d", $post_time_stamp);
    $delta_time = $cur_time - $post_time_stamp;
    $bage_text = __( 'NEW!', 'adventurebeta_theme_dark' );
    $bage = '<span class="new_bage">' . $bage_text . '</span>';
    $output = $delta_time <= 604800 ? $bage : '';
    return $output;
}
// add hew bage to games end

// count amount of views of a game cpt start
function save_game_views( $postID ) {
        
    $metaKey = 'voxel_theme_game_views';
    $views = get_post_meta( $postID, $metaKey, true );
    
    $count = ( empty( $views ) ? 0 : $views );
    $count++;
    
    update_post_meta( $postID, $metaKey, $count );
    
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
// count amount of views of a game cpt end

//exclude from search

function my_post_queries( $query ) {
    // do not alter the query on wp-admin pages and only alter it if it's the main query

    if (!is_admin() && $query->is_main_query()){

        if (is_search()) {
            $query->set('post_type', array('games'));
        }
    }
}
add_action( 'pre_get_posts', 'my_post_queries' );
