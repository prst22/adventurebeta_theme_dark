

		<footer>
			<div class="container grid-xl">
			  <div class="columns">
			    <div class="column col-sm-12 col-md-4 col-lg-3 col-xl-3 col-3">
			    	<div class="footer_logo h">
						
						<?php 

							$args = array( 
									'post_type'		=> 'games',
			        				'post_status'	=> 'publish',
			        				'orderby'		=> 'rand',
			        				'order'			=> 'ASC',
									'posts_per_page' => 1,
									'no_found_rows' => true, 
									'update_post_meta_cache' => false, 
									'update_post_term_cache' => false, 
									'fields' => 'ids'
								 );
							$loop = new WP_Query( $args );
							if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
						?>
							<a href="<?php the_permalink(); ?>" class="btn btn-primary rand_btn" title="Random game"><span class="pr-1"><?php echo __( 'Random', 'adventurebeta_theme' ); ?></span><span class="icon icon-dice-with-five-dots"></span></a>
						<?php	
								wp_reset_postdata();
								
								endwhile;
							endif;

						 ?>
						
					</div>
			    </div>
			    <div class="column col-sm-12 col-md-4 col-lg-6 col-xl-6 col-6">
			    	<div class="bottom_copy">
						<a href="<?php echo home_url();?>" title="Go home" class="main_site_logo">
							<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/logo.png'); ?>" alt="<?php bloginfo('name'); ?>">
						</a>
					</div> 
			    </div>
			    <div class="column col-sm-12 col-md-4 col-lg-3 col-xl-3 col-3">
			    	<div class="scroll_top_cnt h">
			    		<button class="btn btn-primary btn-action" id="jump"><span class="icon icon-arrow-up-thick"></span></button>
			    	</div>
			    </div>
			  </div>
			</div>
			
		</footer>
	</div><!-- site_wrap -->
</div><!--menu wrapper for blur effect -->

<!-- mobile menu start -->
<?php if ( has_nav_menu( 'mobile_menu' ) ) : ?>
	<section class="mobile_menu_wrapper">

		<nav class="mobile_menu_cnt hidden">
			<div class="mobile_menu_cnt__inner hidden">
				<div class="menu_content">
					<h1>MENU</h1>
	                <div class="close_menu_cnt">
	                	<button class="btn btn-primary btn-action" id="close_menu_btn">
	                		<span class="icon icon-close"></span>
	                	</button>
						
	                </div>
					<?php
					   wp_nav_menu( 
                           array(
							   'menu'              => 'Mobile menu',
							   'theme_location'    => 'modile_menu',
							   'container_class'   => 'mobile_menu_list_cnt',
							   'menu_class'        => 'mobile_menu_list_cnt__mobile_list',
                               'depth'             => 1, 
                               'walker'            => new Walkernav()              
						   )
					   );
					?>
				</div>
			</div>
		</nav>

	</section> 
<?php endif; ?>
<!-- mobile menu end -->
</div><!--menu wrapper for blur effect search -->

<?php wp_footer(); ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9NV85"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>