<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
		<?php bloginfo('name');?> |
		<?php is_front_page() ? bloginfo('description') : wp_title('');?> 
	</title>
	<meta name="keywords" content="Games, minecraft, 3d games, voxel games">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php wp_head(); ?>
	<style>
		/*reveal start*/
		html.sr .index_game, html.sr .showcase_right__item, html.sr .showcase_left__item{
		  	visibility: hidden;
		}
		/*reveal end*/
		.glide__slide--one, .glide__slide--two, .glide__slide--three{
			height: 620px;
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center center;
		}
		.glide__slide--one{
			background-image: url(<?php echo ( ( get_theme_mod( 'slide_one' ) > 0 ) ? wp_get_attachment_url( get_theme_mod('slide_one') ) : get_template_directory_uri() . '/assets/showcase_default_dark.jpg' ) ?>); 
		}
		.glide__slide--two{
			background-image: url(<?php echo ( ( get_theme_mod( 'slide_two' ) > 0 ) ? wp_get_attachment_url( get_theme_mod('slide_two') ) : get_template_directory_uri() . '/assets/showcase_default_dark_2.jpg' ) ?>); 
		}
	</style>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T9NV85');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-62372492-3', 'auto',{ 'allowLinker': true});  
    ga('require', 'linker');
    ga('linker:autoLink', ['adventurebox.info','adventurebox.club','adventurebox.live','adventurebox.online','adventure-box.online','adventureboxgames.online','make-a-game.online','playadventurebox.com','adventurebox.com'] );''
    ga('send', 'pageview');
    
    </script>
    <!-- End Google Analytics -->
</head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151885973-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-151885973-1');
</script>
<body <?php body_class(); ?>>
<section class="search_block hidden_search" id="search_block">
	<div class="search_block__close_search_cnt" id="close_search">
		<button class="btn btn-primary btn-action">
			<span class="icon icon-close"></span>
		</button>
	</div>
	<div class="search_block__inner">
		<img class="search_block_img" src="<?php echo esc_url(get_template_directory_uri() . '/assets/logo.png'); ?>" alt="face">
    	<?php get_search_form(); ?>
	</div>	
</section>	
<div class="overflow-search"><!--menu wrapper for blur effect search -->

<div class="overflow"><!--menu wrapper for blur effect -->
	<div class="site_wrap">

		<section class="container site_header_cnt">
			<div class="columns top_bar_background">
                
				<div class="column col-sm-9 col-md-7 col-lg-2 col-xl-2 col-2">
					<header class="site_header_cnt__site_header">
						<a href="<?php echo home_url();?>" title="Go home" class="main_site_logo">
							<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/logo.png'); ?>" alt="<?php bloginfo('name'); ?>">
						</a>
					</header>
				</div>
				
                <div class="column col-sm-3 col-md-5 col-8">
                    <nav class="nav nav_one">
                        <?php if ( has_nav_menu( 'mobile_menu' ) ) : ?>
                            <div class="mobile_menu_toggle show-md hide-lg hide-xl">
                                <div class="mobile_menu_toggle__button">
                                    <button class="btn btn-primary btn-lg btn-action" id="mobile_menu_btn">
                                        <span class="icon icon-th-menu"></span>
                                    </button>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php
                            wp_nav_menu( 
                                array(
                                    'menu'              => 'Main menu',
                                    'theme_location'    => 'main_menu_one',
                                    'menu_class'        => 'top_menu_one',
                                    'depth'             => 1,          
                                    'container_class'   => 'nav__inner hide-md',
                                    'walker'            => new Walkernav()                  
                                )
                            );
                        ?>
                    </nav>
                </div>
                <div class="column hide-md col-2"></div>
                
			</div>
		</section>
		