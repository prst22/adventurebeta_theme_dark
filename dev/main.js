import Glide, { Controls, Autoplay, Keyboard, Swipe } from '@glidejs/glide/dist/glide.modular.esm'
import jump from 'jump.js'
import 'promise-polyfill/src/polyfill'
import 'whatwg-fetch'

// closest() polyfill start
if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector || 
                              Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
  Element.prototype.closest = function(s) {
    var el = this;

    do {
      if (el.matches(s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
}
// closest() polyfill end

// underscore js part start

let o = function(obj) {
    if (obj instanceof o) return obj;
    if (!(this instanceof o)) return new o(obj);
    this.owrapped = obj;
};
o.now = Date.now || function() {
    return new Date().getTime();
};
o.throttle = function(func, wait, options) {
    let timeout, context, args, result;
    let previous = 0;
    if (!options) options = {};

    let later = function() {
        previous = options.leading === false ? 0 : o.now();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
    };

    let throttled = function() {
        let now = o.now();
        if (!previous && options.leading === false) previous = now;
        let remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
        } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };

    throttled.cancel = function() {
        clearTimeout(timeout);
        previous = 0;
        timeout = context = args = null;
    };

    return throttled;
};

// underscore js part end

// CONCATENATED MODULE: ./some_theme/dev/main-wp.js

const  btn = document.getElementById('jump'),
topBar = document.querySelector('.top_bar_background'),
sliderCnt = document.querySelector('.top_slider'),
btnGoToGames = document.getElementById('go_to_games'),
searchTrigger = document.querySelectorAll('.search_trigger>a'),
searchCnt =  document.getElementById('search_block'),
closeSearch = document.getElementById('close_search'),
menuBtn = document.getElementById('mobile_menu_btn'),
closeMenuBtn = document.getElementById('close_menu_btn'),
mobileMenu = document.querySelector('.mobile_menu_cnt'),
mobileMenuList = document.querySelector('.mobile_menu_cnt__inner');
let slider;

// top bar start

let throttledShowTopBAr = o.throttle(() => {
    showHideTopBar();
}, 200, { leading: true, trailing: true });

function showHideTopBar(){
    let scrollBarPos = document.documentElement.scrollTop ? document.documentElement.scrollTop : window.pageYOffset;
    if(scrollBarPos > 78){
        topBar.classList.add('top_bar_background--bg');
    }else{
        topBar.classList.remove('top_bar_background--bg');
    }
}
window.addEventListener('scroll', () => {
    throttledShowTopBAr();  
});

// top bar end

// slider start

window.addEventListener('load', ()=>{
    if(document.body.classList.contains("home")){
        document.querySelector('.showcase_img_cnt').classList.add('rev_sc');
    }
});

if(sliderCnt){
    slider = new Glide('.glide', {
        autoplay: 4500,
        gap: 0,
        hoverpause: true,
        animationDuration: 750,
        animationTimingFunc: 'cubic-bezier(0.445, 0.050, 0.550, 0.950)',
    });
    window.addEventListener('load', ()=>{
        slider.mount({Controls, Autoplay, Keyboard, Swipe});
    });
}

//slider end

btn.addEventListener('click', ()=>{
	jump('body', {
        duration: 1000,
        offset: 0,
	});
});

if(btnGoToGames){
  btnGoToGames.addEventListener('click', (e)=>{
    e.preventDefault();
    jump('.games_to_play', {
        duration: 1000,
        offset: 0,
    });
  });
}

let slideUp = {
    ease: 'cubic-bezier(0.445, 0.050, 0.550, 0.950)',
    origin: 'top',
    scale: 0.9,
    distance: '10%',
    opacity: 0,
    delay: 60,
    duration: 1250,
    interval: 105
};

ScrollReveal().reveal('.index_game', slideUp);

// mobile menu start
function menuOn(){
  jQuery(document).ready(function($) {
    $(function() {  
        $(".mobile_menu_cnt__inner").niceScroll({
          cursorcolor: "#ef594b",
          cursorborder: "1px solid #ef594b",
          autohidemode: false,
          cursorwidth: "4px",
          cursorborderradius: 3,
          zindex: 800
        });
    });
  }); 
}
if(!window.matchMedia("(min-width: 901px)").matches){
    menuOn();
}

if(menuBtn){
    menuBtn.addEventListener('click', toggleMobileMenu);
}

if(menuBtn && mobileMenu){
    closeMenuBtn.addEventListener('click', toggleMobileMenu);
}

if(mobileMenu){
  function controllVpSize(x) {
    if (x.matches && document.body.classList.contains('mobile_menu_active')) { // If media query matches
      toggleMobileMenu(); 
    } 
  }

  function initializeMobileMenu(x) {
    if (!x.matches) { 
      menuOn();
    } 
  }

  let x = window.matchMedia("(min-width: 901px)");
  controllVpSize(x); // Call listener function at run time
  x.addListener(controllVpSize);
  x.addListener(initializeMobileMenu);
  document.addEventListener('keydown', closeMenuOnEsc);
}

function enableDisableSlider(){
  if(document.body.classList.contains("home")){
    if(slider && (document.body.classList.contains('mobile_menu_active') || document.body.classList.contains('search_active'))){
      if(!slider.disabled) slider.disable();
    }else if(slider && (!document.body.classList.contains('mobile_menu_active') || !document.body.classList.contains('search_active'))){
      if(slider.disabled) slider.enable();
    }
  }
}

function toggleMobileMenu(e){
  function openCloseMenuWithNoTransitionDelay(){
    mobileMenu.classList.remove('transition_delay');
    document.body.classList.toggle('mobile_menu_active');
    mobileMenu.classList.toggle('hidden');
    mobileMenuList.classList.toggle('hidden');
    enableDisableSlider();
  }
  if(e){ //check event to target click 
    if(e.target.closest('button') === closeMenuBtn 
      || e.key == "Escape" 
      || e.key == "Esc" 
      || e.keyCode == 27){ //choose open or close or esc button to determin whether to use transition delay
      document.body.classList.toggle('mobile_menu_active');
      mobileMenu.classList.add('transition_delay');
      mobileMenu.classList.toggle('hidden');
      mobileMenuList.classList.toggle('hidden');
      enableDisableSlider();
    }else{
      openCloseMenuWithNoTransitionDelay();
    }
  }else{
    openCloseMenuWithNoTransitionDelay();
  }
  mobileMenu.addEventListener(transitionEvent, ()=>{
    jQuery(document).ready(function($) {
        $('.mobile_menu_cnt__inner').getNiceScroll().resize();
    });
  }); 
}

function closeMenuOnEsc(evt){
    evt = evt || window.event;
    let isEscape = false;
    if("key" in evt){
      isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
      isEscape = (evt.keyCode == 27);
    }
    if(isEscape && document.body.classList.contains('mobile_menu_active') 
      && !document.body.classList.contains('search_active')){
      toggleMobileMenu(evt);
    }

    if(isEscape && document.body.classList.contains('mobile_menu_active') 
      && document.body.classList.contains('search_active')){
      closeSearchModal();
    }
}
function whichTransitionEvent(){
  let t,
      el = document.createElement("fakeelement");

  let transitions = {
    "transition"      : "transitionend",
    "OTransition"     : "oTransitionEnd",
    "MozTransition"   : "transitionend",
    "WebkitTransition": "webkitTransitionEnd"
  }

  for (t in transitions){
    if (el.style[t] !== undefined){
      return transitions[t];
    }
  }
}

let transitionEvent = whichTransitionEvent();

// mobile menu end

// sorting start

if(document.body.classList.contains('home')){

  history.replaceState({page: location.href}, null, location.href);

  const ajaxUrl = document.querySelector('.sorting_type').getAttribute('data-url'),
  gamesContainer = document.getElementsByClassName('games_cnt')[0],
  sortBtnNew = document.getElementsByClassName('sort_new')[0],
  gameTypeBtn = document.getElementsByClassName('btn_game_type'),
  sortBtnPopular = document.getElementsByClassName('sort_popular')[0];
  let paginationCnt = document.getElementsByClassName('pagination_links')[0];
  let updatePagination = function (){
    let pagLinks = document.querySelectorAll('.pagination_links>a.page-numbers');
    for(let i = 0; i < pagLinks.length; i++){
      pagLinks[i].addEventListener('click', goToNewPage);
    }
  };
  
  let whichEvent = (event) => {
    let whatEvent = event.type,
    targetPaginationPage;
    if(whatEvent == 'click'){
      event.preventDefault();
      return targetPaginationPage = (!isNaN(Number(event.target.textContent))) ? Number(event.target.textContent) : 1;
    } else if(whatEvent == 'popstate') {
      let pat = /\/page\/(\d+)/;
      let num = event.state.page.match(pat);
      return targetPaginationPage = (num !== null) ? num[1] : 1;
    }
  };

  let loaderHideShow = (e) => {
    let gamesLoader = document.querySelector('.loader_overlay'),
    sortingBarBtns = document.querySelectorAll('.sorting_cnt .btn');
    // sortBtnNew.disabled = !sortBtnNew.disabled;
    // sortBtnPopular.disabled = !sortBtnPopular.disabled;
    for (var i = sortingBarBtns.length - 1; i >= 0; i--) {
      sortingBarBtns[i].disabled = !sortingBarBtns[i].disabled;
    }
    if(e.target.classList.contains('sort_new') || e.target.classList.contains('sort_popular')){
      e.target.classList.toggle('loading');
    }else if(e.target.classList.contains('btn_game_type')){
      e.target.classList.toggle('loading_btn');
    }
    if(gamesLoader) gamesLoader.classList.toggle('hidden_loader');
  };

  let goToNewPage = (e) => {
    // adding active class to target data type for fetch start
    if(e.target.classList.contains('sort_new') || e.target.classList.contains('sort_popular')){
      let sortBtns = document.querySelectorAll('.btn.secondary_btn');
      for (var i = sortBtns.length - 1; i >= 0; i--) {
        sortBtns[i].classList.remove('active');
      }
      e.target.classList.add('active');
    }
    if(e.target.classList.contains('btn_game_type')){
      for (var i = gameTypeBtn.length - 1; i >= 0; i--) {
        gameTypeBtn[i].classList.remove('active');
      }
      e.target.classList.add('active');
    }
    // adding active class to target data type for fetch end
    loaderHideShow(e);
    let formData = new FormData(),
    sortingKey = document.querySelector('.sorting_type .active').getAttribute('data-type'),
    gameTypes = document.querySelector('.game_types_cnt .btn_game_type.active').getAttribute('data-type'),
    targetPage = whichEvent(e);
    formData.append('page_number', targetPage);
    formData.append('meta_key', sortingKey);
    formData.append('tax_query_terms', gameTypes);
    formData.append('action', 'sort_games');

    let options = {
      method: 'POST',
      body: formData
    },
    req = new Request(ajaxUrl, options);

    let newGamePage = makeSortingRequest(e, req);

    newGamePage.then(newGameposts => {
      if(newGameposts !== '' && newGameposts !== undefined){
        gamesContainer.innerHTML = '';
        gamesContainer.innerHTML = newGameposts; 
      }else{
        throw new Error('Something went wrong, pls refresh the page'); 
      }
    })
    .then(page => {
      if(e.type !== 'popstate') updateUrl(document.getElementsByClassName('pagination_links')[0]);
      paginationCnt = document.getElementsByClassName('pagination_links')[0];
      if(paginationCnt) updatePagination();
      ScrollReveal().sync();
      loaderHideShow(e);
    })
    .catch((err) =>{
      alert(err.message);
      loaderHideShow(e);
    });
  };

  let makeSortingRequest = (e, req) => {

      let result = fetch(req)
      .then((response)=>{
        if(response.ok){
          return response.text();
        }else{
          throw new Error('Something went wrong, pls refresh the page'); 
        }
      })
      .catch((err) =>{
        if(err.message){
          console.log(err.message);
        }else{
          console.log('Something went wrong, pls refresh the page');
        }
      });
      
      return result;
  };

  function updateUrl(element){
    let previousPageData = element.getAttribute('data-page');
    history.pushState({ page: element.getAttribute('data-page') }, null, element.getAttribute('data-page'));
    return false;
  }
  
  window.addEventListener('popstate', (event) => {
    if(event.state === null){
      event.preventDefault();
      return false;
    }
    let character = event;
    if(character) goToNewPage(character);
  });

  updatePagination();
  // attach events to game sorting bar start
  if(gameTypeBtn){
    for (let i = gameTypeBtn.length - 1; i >= 0; i--) {
      gameTypeBtn[i].addEventListener('click', goToNewPage);
    }
  }
  sortBtnNew.addEventListener('click', goToNewPage);
  sortBtnPopular.addEventListener('click', goToNewPage);
  // attach events to game sorting bar end
}

// sorting end

// search modal start

if(searchTrigger.length != 0){
  for (let i = 0; i < searchTrigger.length; i++) {
    searchTrigger[i].addEventListener('click', (e) =>{
      e.preventDefault();
      closeSearchModal();
    });
  }

  closeSearch.addEventListener('click', (e) =>{
    closeSearchModal();
  });
}
function closeSearchModal(){
  document.addEventListener('keydown', closeSearchOnEsc);
  document.body.classList.toggle('search_active', !document.body.classList.contains('search_active'));
  searchCnt.classList.toggle('hidden_search', !searchCnt.classList.contains('hidden_search'));
  enableDisableSlider();
}
function closeSearchOnEsc(evt){
  if(document.body.classList.contains('mobile_menu_active') ){
    return
  }
  evt = evt || window.event;
  let isEscape = false;
  if("key" in evt){
      isEscape = (evt.key == "Escape" || evt.key == "Esc");
  }else{
      isEscape = (evt.keyCode == 27);
  }
  if(isEscape && document.body.classList.contains('search_active')){
    document.body.classList.remove('search_active');
    searchCnt.classList.add('hidden_search');
    document.removeEventListener('keydown', closeSearchOnEsc);
    enableDisableSlider();
  } 
}

// search modal end

//add to favourite start

if(document.body.classList.contains('games-template-default')){

  // adblock detection notice start

  if( window.canRunAds === undefined ){
  // adblocker detected, show fallback
    let gameContainer = document.getElementsByClassName('single_post')[0],
    toastOuterCnt = document.createElement('div'),
    toastCnt = document.createElement('div'),
    removeToastBtn = document.createElement('button'),
    toastTextCnt = document.createElement('p'),
    toastHeading = document.createElement('h4'),
    toastHeadingText = document.createTextNode("AdBlock detected"),
    toastReloadLink = document.createElement('a'),
    toastText = `Please note that it is necessary to disable adblock to play a game.
    Disable adblock and <a href="${window.location.href}">reload page</a>.`;
    toastOuterCnt.className = 'adblock_notice';
    toastCnt.className = 'toast toast-primary';
    removeToastBtn.className = 'btn btn-clear float-right close_notice';

    toastTextCnt.innerHTML = toastText;
    toastHeading.appendChild(toastHeadingText);

    toastOuterCnt.appendChild(toastCnt);
    toastCnt.appendChild(toastHeading);
    toastCnt.appendChild(removeToastBtn);
    toastCnt.appendChild(removeToastBtn);
    toastCnt.appendChild(toastTextCnt);

    document.getElementsByClassName('single_post_main__inner')[0].insertBefore(toastOuterCnt, gameContainer);

    if(removeToastBtn){
      removeToastBtn.addEventListener('click', ()=>{
        toastOuterCnt.style.maxHeight = '0px';
      });
      toastOuterCnt.addEventListener(transitionEvent, (e)=>{
        if(e.target.classList.contains('adblock_notice')){
          toastOuterCnt.parentNode.removeChild(toastOuterCnt);
        }
      }, false);
    }
    

  }
  // adblock detection notice end

}





