<?php

	$term = get_term( get_the_ID() );
	$cur_time = time();
        
 ?>
<article <?php post_class( array('index_game', 'column', 'col-sm-12', 'col-6') ); ?>>
	<div class="index_game__inner">
		<?php 
			$post_date = get_the_date( 'U', get_the_ID() );
			echo voxel_new_badge($post_date, $cur_time, get_the_ID());
			$views = get_post_meta(get_the_ID(), 'voxel_theme_game_views', TRUE);
		?>
		<div class="index_game__inner--bg">

			<?php if(has_post_thumbnail()): ?><!-- has thumb start -->
				<figure class="thumbnail_cnt">
					<span class="thumbnail_cnt__game_views" title="game views">
						<span class="icon icon-eye"></span>
						<?php 
							$print_views = empty($views) ? 0 : $views;
							echo "<span>{$print_views}</span>";
						?>
					</span>
					<div class="thumbnail_cnt__inner">

						<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link">
							<?php 
								the_post_thumbnail('game-thumbnail', 
								    $attr = array( 
										'class' => "thumbnail",
										'alt' => esc_attr(get_the_title()),
										'sizes' => '(max-width: 480px) 340px, (max-width: 620px) 550px, (max-width: 740px) 340px, (max-width: 1190px) 540px, 650px'
									)
								); 
							?>
                        </a> 
                        
                        <?php if(get_post_type() === 'games'): ?>

                            <div class="pad_outer">

                                <div class="pad">
                                    <span class="icon icon-gamepad"></span>
                                </div>
                                <?php
                                    $game_types = get_the_terms( get_the_ID(), 'game_types' );

                                    if(!empty($game_types)):

                                        foreach( $game_types as $type ) {
                                            
                                            if($type->slug == 'battlefield'){
                                                $game_type_icon = sprintf(

                                                    '<div class="pad pad--type">
                                                        <a href="%1$s" title="%2$s">
                                                            <span class="icon icon-revolver"></span>
                                                        </a>
                                                        </div>',
                                                    esc_url( get_category_link( $type->term_id ) ),
                                                    esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta_theme_dark' ), $type->name ) ),
                                                    esc_html__($type->count),
                                                    esc_html( $type->name )
                                                );

                                                echo $game_type_icon; 
                                            }

                                            if($type->slug == 'open-world'){
                                                $game_type_icon = sprintf(

                                                    '<div class="pad pad--type">
                                                        <a href="%1$s" title="%2$s">
                                                            <span class="icon icon-earth"></span>
                                                        </a>
                                                        </div>',
                                                    esc_url( get_category_link( $type->term_id ) ),
                                                    esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta_theme_dark' ), $type->name ) ),
                                                    esc_html__($type->count),
                                                    esc_html( $type->name )
                                                );

                                                echo $game_type_icon; 
                                            }

                                            if($type->slug == 'other'){
                                                $game_type_icon = sprintf(

                                                    '<div class="pad pad--type">
                                                        <a href="%1$s" title="%2$s">
                                                            <span class="icon icon-dots-three-horizontal"></span>
                                                        </a>
                                                        </div>',
                                                    esc_url( get_category_link( $type->term_id ) ),
                                                    esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta_theme_dark' ), $type->name ) ),
                                                    esc_html__($type->count),
                                                    esc_html( $type->name )
                                                );

                                                echo $game_type_icon; 
                                            }
                                            
                                        } 
                                    endif;
                                ?>
                                
                            </div>
                            <?php endif; ?>           

                        <!-- thum short description start -->

                        <div class="game_description">
                            <h4 class="game_description__heading">
                                <a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                            </h4>
                            
                            <div class="game_description__excerpt">
                                <?php the_excerpt(); ?>
                            </div>

                            <?php if(get_post_type() === 'games'): ?>
                                <div class="game_description__play">
                                    <a class="btn btn-primary btn_play" href="<?php the_permalink(); ?>">PLAY</a>
                                </div>
                                <?php else: ?>
                                <div class="game_description__play">
                                    <a class="btn btn-primary btn_play" href="<?php the_permalink(); ?>">READ MORE</a>
                                </div>
                            <?php endif; ?>
                        </div>

                        <!-- thum short description end -->                 

                    </div>
                    
				</figure> 			

			<?php endif; ?><!-- has thumb end -->

		</div>
	</div>
</article>