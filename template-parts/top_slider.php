<?php
    $top_game_args = array(
        'post_type'	=> 'games',
        'posts_per_page' => 1,
        'post_status' => 'publish',
        'no_found_rows' => true, 
        'update_post_meta_cache' => false, 
        'update_post_term_cache' => false, 
        'tax_query' => array(
            array(
                'taxonomy' 	=> 'game_tags',
                'field' 	=> 'slug',
                'terms'    	=> 'Featured game',
            )
        )
    );
    $top_game_args_second_game = array(
        'post_type'	=> 'games',
        'posts_per_page' => 1,
        'post_status' => 'publish',
        'offset' => 1,
        'no_found_rows' => true, 
        'update_post_meta_cache' => false, 
        'update_post_term_cache' => false, 
        'tax_query' => array(
            array(
                'taxonomy' 	=> 'game_tags',
                'field' 	=> 'slug',
                'terms'    	=> 'Featured game',
            )
        )
    );
    $top_game_query = new WP_Query($top_game_args);
    $top_game_query_two = new WP_Query($top_game_args_second_game);
?>
<section class="sc loading loading-lg">
	<div class="showcase_img_cnt">
		<section class="top_slider">
			<div class="glide">
				<div class="glide__track" data-glide-el="track">
					<ul class="glide__slides">
						<li class="glide__slide glide__slide--one">
							<div class="slider_info">
								<div class="slider_info__left">
                                    <div class="first_slide_game">
                                        <?php 

                                            if ( $top_game_query->have_posts() ):
                                                while ( $top_game_query->have_posts() ): ?>

                                                    <?php $top_game_query->the_post(); ?>

                                                    <div class="first_slide_game__info">
                                                        <h3>
                                                            <?php the_title(); ?>
                                                        </h3> 
                                                    
                                                        <?php the_excerpt(); ?>

                                                    </div>
                                                    <?php get_template_part( 'template-parts/one', 'slider-game' ); ?>
                                        <?php 

                                            wp_reset_postdata();
                                            endwhile; 

                                        ?>
                                        <?php endif; ?>
                                    </div>
								</div>
								<div class="slider_info__right">
                                    <a href="https://www.adventurebox.com/maker" target="_blank" class="btn btn-primary btn-lg">Make A Game</a>
                                    <p>Press any of the headlines below to sort the games</p>
									<a href="#" class="btn btn-primary btn-sm" id="go_to_games">
                                        GAMES TO PLAY
                                        <span class="icon icon-arrow-down-thick"></span>
                                    </a>
								</div>
							</div>	
						</li>
						<li class="glide__slide glide__slide--two">
							<?php 

								if ( $top_game_query_two->have_posts() ):
									while ( $top_game_query_two->have_posts() ): ?>

										<?php $top_game_query_two->the_post(); ?>

										<div class="slider_info">
											<div class="slider_info__left">
												<h1 class="slider_heading">
													<?php the_title(); ?>
												</h1> 
												<h4>
													<?php the_excerpt(); ?>
												</h4>
											</div>
											<div class="slider_info__right">
												<?php get_template_part( 'template-parts/one', 'slider-game' ); ?>
											</div>
										</div>
									<?php 
										wp_reset_postdata();
										endwhile; 
									?>
									<?php else: ?>
									<div class="slider_info">
										<div class="slider_info__left slider_info__left--no_game">
											<h1><?php echo get_bloginfo('description'); ?></h1>
										</div>
									</div>
									
								<?php endif; ?>
						</li>
					</ul>
				</div>

				<div class="glide__bullets" data-glide-el="controls[nav]">
					<button class="glide__bullet" data-glide-dir="=0"></button>
					<button class="glide__bullet" data-glide-dir="=1"></button>
				</div>
			</div>
		</section>
	</div>
</section>