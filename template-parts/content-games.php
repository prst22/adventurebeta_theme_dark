<?php 
	$views = get_post_meta(get_the_ID(), 'voxel_theme_game_views', TRUE);
	$iframe_link = get_post_meta($post->ID, '_voxel_games_iframe_link_value_key', true);
	$game_types = get_the_terms( get_the_ID(), 'game_types' );
  ?>
<article <?php post_class( array( 'single_post' ) ); ?>>
	<div class="game_description_single">
		<div class="single_post__info">	
			
			<div class="game_types_cnt_single single_views">
				<div class="game_types_cnt_single__inner">
				<span class="icon icon-eye"></span>
					<?php 
						
						$print_views = empty($views) ? 0 : $views;
						echo '<span class="count">' . $print_views . '</span>';
					?>
				</div>
			</div>

			<?php if(!empty($game_types)): ?>		
				<div class="game_types_cnt_single">
					<div class="game_types_cnt_single__inner">
					<?php

						foreach( $game_types as $type ) {
							
							if($type->slug == 'battlefield'){
								$game_type_icon = sprintf(

							        '<span class="single_post_type_icon">
							        	<a href="%1$s" title="%2$s">
							        		<span class="icon icon-revolver"></span>
							        	</a>
							        	</span>',
							        esc_url( get_category_link( $type->term_id ) ),
							        esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta_theme_dark' ), $type->name ) ),
							        esc_html__($type->count),
							        esc_html( $type->name )
							    );
	 
								echo $game_type_icon; 
							}

							if($type->slug == 'open-world'){
								$game_type_icon = sprintf(

							        '<span class="single_post_type_icon">
							        	<a href="%1$s" title="%2$s">
							        		<span class="icon icon-earth"></span>
							        	</a>
							        	</span>',
							        esc_url( get_category_link( $type->term_id ) ),
							        esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta_theme_dark' ), $type->name ) ),
							        esc_html__($type->count),
							        esc_html( $type->name )
							    );
	 
								echo $game_type_icon; 
							}

							if($type->slug == 'other'){
								$game_type_icon = sprintf(

							        '<span class="single_post_type_icon">
							        	<a href="%1$s" title="%2$s">
							        		<span class="icon icon-dots-three-horizontal"></span>
							        	</a>
							        	</span>',
							        esc_url( get_category_link( $type->term_id ) ),
							        esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta_theme_dark' ), $type->name ) ),
							        esc_html__($type->count),
							        esc_html( $type->name )
							    );
	 
								echo $game_type_icon; 
							}
						     
						} 
						
					?>
					</div>
				</div>
			<?php endif; ?>
			
		</div>
	</div>
	
</article>
<?php if(isset($iframe_link) && !empty($iframe_link)): ?>
	<div class="iframe_wrapper">
		<div class="iframe_wrapper__game">
		<iframe id="game_frame" src="<?php echo esc_attr($iframe_link); ?>" frameBorder="0" allowfullscreen="true" importance="high" scrolling="no"></iframe>
		</div>
	</div>
<?php endif; ?>