<?php 

	add_action('wp_ajax_nopriv_sort_games', 'sort_games');
	add_action('wp_ajax_sort_games', 'sort_games');

	function sort_games(){

		if(wp_doing_ajax()){
			$page_number = wp_strip_all_tags( $_POST["page_number"] );
			$game_types = wp_strip_all_tags( $_POST["tax_query_terms"] );
			$page = ( $page_number == (int)$page_number ) ? (int)$page_number : 1;
			$meta_key_popularity = wp_strip_all_tags( $_POST["meta_key"] ) == 'sort_popularity' ?: '';
			$meta_key = !empty($meta_key_popularity) ? 'voxel_theme_game_views' : '';
			$order_by = !empty($meta_key_popularity) ? 'meta_value_num date' : 'date';
			$page_trail = get_site_url(null , null , 'relative') .'/';
			if($meta_key_popularity == 'sort_popularity'){
				$_SESSION['sort_type'] = 'popularity';
			}else{
				$_SESSION['sort_type'] = 'new';
			}
			$types_of_games_arr = array();

			$categories = get_categories(
				array(
	            	'taxonomy' => 'game_types',
				    'orderby'  => 'name',
				    'order'    => 'ASC'
				)
			);
			// fill tax query terms to determin game types start
			if($game_types == 'all'){
				foreach ($categories as $category) {
					$types_of_games[] = $category->slug;
				}
				$_SESSION['game_types'] = $types_of_games;
			}else{
				$types_of_games[] = $game_types;
				$_SESSION['game_types'] = $types_of_games;
			}
			// fill tax query terms to determin game types start
			$args = array(
		        'post_type'		=> 'games',
		        'orderby' 		=> $order_by,
				'meta_key'		=> $meta_key,
		        'post_status'	=> 'publish',
		        'paged' 		=> $page,
		        'order'     	=> 'DESC',
		        'tax_query' 	=> array(
					array(
						'taxonomy' 	=> 'game_types',
						'field' 	=> 'slug',
						'terms'    	=> $types_of_games,
					)
				)
		    );
			
			$sorted_games_query = new WP_Query($args);

			if ( $sorted_games_query->have_posts() ) {
				echo '<div class="loader_overlay"><div class="loading loading-lg"></div></div>';
				while ( $sorted_games_query->have_posts() ) {
					$sorted_games_query->the_post();			        
					get_template_part( 'template-parts/one', 'post' ); 
				} // end while
				
			}else{
				echo '<div class="loader_overlay"><div class="loading loading-lg"></div></div>';
				get_template_part( 'template-parts/one', 'nopost' );
			}
			
			wp_reset_postdata();

			if($sorted_games_query->max_num_pages > 0): 

				echo '<div class="column col-12">
					<div class="pagination_cnt pagination_links pagination_links--numbered" data-page="' . $page_trail . 'page/'. $page .'">';
				
						echo paginate_links(
							array(
								'base'               => '#',
								'format'             => '',
								'prev_next'          => false,
								'type'               => 'plain',
								'total'              => $sorted_games_query->max_num_pages,
								'current'            => max( 1, $page ),
								'end_size'           => 2,
								'mid_size'           => 2,
							)
						);

					echo '</div>
				</div>';
					
			endif;
		}

		wp_die();
	}

	function if_paged($num = null){
		$output = '';
		if(is_paged()){ $output = 'page/'. get_query_var('page');}

		if($num == 1){
			$num_p = ( get_query_var('page') == 0 ? 1 : get_query_var('page') );
            return $num_p;
		}else{
			return $output;
		}
	}