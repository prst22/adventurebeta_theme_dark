<?php
    class Walkernav extends Walker_Nav_Menu{
        public $count;
        public $seaechIcon;
        
        public function __construct(){
            $this->count = 0;
            $this->seaechIcon = sprintf('<li class="search_trigger"><a href="#">%s</a></li>',  '<span class="icon icon-search menu_search_icon"></span>');
        }
        
        public function end_el( &$output, $item, $depth = 0, $args = null ){

            if($depth === 0){
                $this->count = $this->count + 1;
            }
            
            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            if($this->count === 2 && $depth === 0){
                $output .=  $this->seaechIcon . "</li>{$n}";
            }

            $output .= "</li>{$n}";
            
        }
    }