<?php  
	function adventurebeta_theme_customizer_register($wp_customize){
        $wp_customize->add_section('site_slider', 
            array(
                'title'				=> __('Site slider images', 'adventurebeta_theme_dark'),
                'description'		=> sprintf(__('Site slider', 'adventurebeta_theme_dark')),
                'priority'			=> 10
            ));
		$wp_customize-> add_setting('slide_one', 
			array(
				'type' 			=> 'theme_mod',
				'capability'    => 'edit_theme_options',
				'transport'     => 'refresh',
			)
		);
        $wp_customize-> add_control(new WP_Customize_Cropped_Image_Control( $wp_customize, 'slide_one', 
            array(
				'label'        => __( 'Slider image one (min 1920x620)', 'adventurebeta_theme_dark' ),
				'section'      => 'site_slider',
				'settings'     => 'slide_one',
				'flex_width'   => false, // Optional. Default: false
                'flex_height'  => true,
				'width'        => 1920,
                'height'       => 640,
			)
		));
		$wp_customize-> add_setting('slide_two', 
			array(
				'type' 			=> 'theme_mod',
				'capability'    => 'edit_theme_options',
				'transport'     => 'refresh',
			)
		);
		$wp_customize-> add_control(new WP_Customize_Cropped_Image_Control( $wp_customize, 'slide_two', 
            array(
				'label'        => __( 'Slider image two (min 1920x620)', 'adventurebeta_theme_dark' ),
				'section'      => 'site_slider',
				'settings'     => 'slide_two',
				'flex_width'   => false, // Optional. Default: false
                'flex_height'  => true,
				'width'        => 1920,
                'height'       => 640,
			)
		));
	}
	add_action('customize_register', 'adventurebeta_theme_customizer_register');