<?php
	get_header();
	$total_games = wp_count_posts( 'games' )->publish;
	$categories = get_categories(
		array(
        	'taxonomy' => 'game_types',
		    'orderby'  => 'name',
		    'order'    => 'ASC'
		)
	);

	if(isset($_SESSION['game_types']) && !empty($_SESSION['game_types'])){
		$types_of_games = $_SESSION['game_types'];
	}else{
		foreach ($categories as $category) {
			$types_of_games[] = $category->slug;
		}
	}
	
?>

<?php if(is_front_page()):

    get_template_part( 'template-parts/top_slider' ); 

endif; ?>

<main class="container grid-xl main">
	<div class="main__inner">
		<div class="columns sorting_cnt">
			<div class="column col-12">
				<h2>SORT GAMES BY:</h2> 
			</div> 
			<?php

				$count_game_posts = wp_count_posts('games')->publish;

	            if($count_game_posts > 0): ?>

					<div class="column col-lg-12 col-xl-8 col-8">
	            		<div class="game_type_sub_head">
	            			<small>Click the category buttons to sort the games</small>
	            		</div>
	            	 	<div class="game_types_cnt"> 
	            	 		<div class="btn-group btn-group-block">
	            	 			<button class="btn btn-primary badge btn_game_type <?php echo isset($_SESSION['game_types']) ? ( count($_SESSION['game_types']) > 1 ? 'active' : '' ) : 'active'; ?>" data-badge="<?php echo $total_games; ?>" data-type="all"><?php _e('All games', 'adventure-theme'); ?></button>
								<?php

									foreach( $categories as $category ) {
									
									    $category_link = sprintf( 
									        '<button class="btn btn-primary badge btn_game_type %1$s" title="%2$s" data-badge="%3$s" data-type="%4$s">%5$s games</button>',

									        esc_html( isset($_SESSION['game_types']) && count($_SESSION['game_types']) == 1 && $_SESSION['game_types'][0] == $category->slug ? 'active ': '' ),
									        esc_attr( sprintf( __( 'View all games in %s', 'adventure-theme' ), $category->name ) ),
									        esc_html__($category->count),
									        esc_html($category->slug),
									        esc_html($category->name)
									        
									    );

										echo $category_link; 

									} 
								?>
							</div>
						</div>
					</div>

				<?php endif; ?>
				<div class="column col-lg-12 col-xl-4 col-4">
					<div class="game_type_sub_head">
						<small>Choose sorting type</small>
					</div>
					
            	 	<div class="game_types_cnt"> 
            	 		<div class="sorting_type" data-url="<?php echo admin_url('admin-ajax.php'); ?>">

            	 			<div class="btn-group btn-group-block">
							 	<button class="btn btn-primary secondary_btn sort_new <?php echo isset($_SESSION['sort_type']) ? ( ($_SESSION['sort_type'] == 'new') ? 'active' : '' ) : 'active'; ?>" data-type="sort_new">New</button>
								<button href="#" class="btn btn-primary secondary_btn sort_popular <?php echo (isset($_SESSION['sort_type']) && $_SESSION['sort_type'] == 'popularity') ? 'active' : ''; ?>" data-type="sort_popularity">Popular</button>
							</div>

            	 		</div>
            	 	</div>
	            	
				</div>
		</div>
		<div class="columns">
			<?php 
				if($count_game_posts > 0){
					echo '<div class="column col-12 games_to_play"><h2>GAMES TO PLAY:</h2></div>';
				} 
			?>
		</div>
		<div class="columns games_cnt">
		<?php
			if(isset($_SESSION['sort_type'])){
				$order_by = $_SESSION['sort_type'] == 'new' ? 'date' : 'meta_value_num date';
				$meta_key = $_SESSION['sort_type'] == 'new' ? '' : 'voxel_theme_game_views';
			}else{
				$order_by = 'date';
				$meta_key = '';
			}
			$args = array(
		        'post_type'		=> 'games',
		        'post_status'	=> 'publish',
		        'orderby' 		=> $order_by,
				'meta_key'		=> $meta_key,
		        'paged' 		=> if_paged(1),
		        'order'     	=> 'DESC',
		        'tax_query' 	=> array(
					array(
						'taxonomy' 	=> 'game_types',
						'field' 	=> 'slug',
						'terms'    	=> $types_of_games,
					)
				)
		    );

			$games_query = new WP_Query($args);

			if ( $games_query->have_posts() ) {
				echo '<div class="loader_overlay hidden_loader"><div class="loading loading-lg"></div></div>';
				while ( $games_query->have_posts() ) {
					$games_query->the_post();			        
					get_template_part( 'template-parts/one', 'post' ); 
				} // end while
				wp_reset_postdata();
			}else{
				echo '<div class="loader_overlay hidden_loader"><div class="loading loading-lg"></div></div>';
				get_template_part( 'template-parts/one', 'nopost' );
			}
			?>	
			
			<?php if($games_query->max_num_pages > 0): ?>	
			
				<div class="column col-12">
					<div class="pagination_cnt pagination_links pagination_links--numbered" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
						
						<?php echo paginate_links(
							array(
									'base'               => '#',
									'format'             => '',
									'prev_next'          => false,
									'type'               => 'plain',
									'total'              => $games_query->max_num_pages,
									'current'            => if_paged(1),
									'end_size'           => 2,
									'mid_size'           => 2,
								)
							); 
						?>
					</div>
				</div>

			<?php endif; ?>
		</div>		
	</div>
</main>

<?php		
	get_footer();
?>