<?php
/* Template Name: Featured games */
?>
<?php get_header(); ?>
    <main class="container grid-xl main">
        <div class="columns single_page main__inner">
            <header class="column col-12 single_page_heading">
                <h1 class="heading_title"><?php the_title(); ?>:</h1>
            </header>
            
            <?php 
                $featured_game_args = array(
                    'post_type'	=> 'games',
                    'posts_per_page' => -1,
                    'post_status' => 'publish',
                    'tax_query' => array(
                        array(
                            'taxonomy' 	=> 'game_tags',
                            'field' 	=> 'slug',
                            'terms'    	=> 'Featured game',
                        )
                    )
                );
                $featured_game_query = new WP_Query($featured_game_args);

                if ( $featured_game_query->have_posts() ):
                    while ( $featured_game_query->have_posts() ): ?>

                        <?php $featured_game_query->the_post(); ?>

                            <?php get_template_part( 'template-parts/one', 'featured-game' ); ?>

                    <?php 
                        wp_reset_postdata();
                        endwhile; 
                    ?>
                    <?php else: ?>

                        <?php get_template_part( 'template-parts/one', 'nopost' ); ?>
                    
                <?php endif; ?> 
        </div>
    </main>
<?php get_footer(); ?>