
<form method="get" class="searchform form-inline search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<?php

		$search_categories = get_categories( 
			array(
		        	'taxonomy' => 'game_types',
				    'orderby'  => 'name',
				    'order'    => 'ASC'
				)
		);
		
	?>
	<?php if(!empty($search_categories)): ?>	

		<div class="form-group mb-2 pb-2 choose_search_cat">
			
			<label class="form-label form-inline pr-2">
				<small>Search in:</small> 
			</label>

			<label class="form-radio form-inline">
			    <input type="radio" id="all" name="term" value="" <?php echo ( empty($_GET['term']) ? 'checked' : '' ); ?> >
			    <i class="form-icon"></i>
			    <small>All games</small>
			</label>

			<?php

				foreach( $search_categories as $category ) {
					echo '<label class="form-radio form-inline">
						    <input type="radio" id="' . $category->slug . '" name="term" value="' . $category->slug . '" ' . ( ( isset($_GET['term']) && $_GET['term'] === $category->slug ) ? 'checked' : '' ) . '>
						    <i class="form-icon"></i>
						    <small>' . ucfirst($category->name) . '</small>
						  </label>';
				} 

			?>
			
		</div>
	<?php endif; ?>
	<div class="input-group">
		<span class="input-group-addon">
			<span class="icon icon-gamepad"></span>
		</span>

	  	<input type="text" class="form-input input-lg search_field" name="s" placeholder="<?php _e( 'Game search', 'adventurebeta_theme_dark' ); ?>" title="search" size="22" value="<?php echo ( !empty(get_search_query()) ? get_search_query() : ''); ?>">

	  	<button class="btn btn-primary btn-lg input-group-btn" type="submit" name="submit">
	  		<span class="icon icon-search"></span>
	  	</button>
	</div>
	<input type="hidden" name="taxonomy" value="game_types">
</form>