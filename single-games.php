<?php get_header(); ?>
<?php
    $terms = wp_get_post_terms( $post->ID, 'game_types'); 
    $terms_ids = [];

    foreach ( $terms as $term ) {
        $terms_ids[] = $term->term_id;
    }
    $cur_id = $post->ID;
 ?>
    <main class="container grid-xl single_post_main">
        <div class="columns">
            <div class="column col-12">
                <?php
                // Start the loop.

                while ( have_posts() ) : the_post();
                    
                    save_game_views(get_the_ID());

                    ?>
                    <div class="single_post_main__inner">

                    <?php
                        get_template_part( 'template-parts/content-games');
                    ?>
                    </div>

                    <?php
                    
                // End the loop.
                endwhile;
                ?>
            </div>
            
        </div>

        <?php 
            $args = array(
                'post_type'                 => 'games',
                'post_status'               => 'publish',
                'order'                     => 'ASC',
                'orderby'                   => 'rand',
                'posts_per_page'            => 6,
                'no_found_rows'             => true, 
                'update_post_meta_cache'    => false, 
                'update_post_term_cache'    => false,
                'post__not_in'              => array($cur_id),
                'tax_query'                 => array(
                    array(
                        'taxonomy'  => 'game_types',
                        'field'     => 'term_id',
                        'terms'     => $terms_ids 
                    )
                )
            );

            $related_query = new WP_Query($args);

            $count = $related_query->post_count;
            if(!empty($terms_ids) && $count > 0){

                echo '<div class="columns related">';

                echo '<h3 class="column col-12">Related Games:</h3>';

                if ( $related_query->have_posts() ):

                    while ( $related_query->have_posts() ): 

                        $related_query->the_post();
                            if(has_post_thumbnail()):
                        ?>

                        <div class="column col-xs-6 col-sm-4 col-sm-4 col-md-3 col-lg-2 col-2">
                            <figure class="thumbnail_cnt thumbnail_cnt--related">
                                <div class="thumbnail_cnt__inner thumbnail_cnt__inner--related">
                                    <a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link thumbnail_cnt__link--related">
                                        <?php 
                                            the_post_thumbnail('game-thumbnail', 
                                            $attr = array(
                                                    'class' => "thumbnail",
                                                    'alt' => esc_attr(get_the_title()),
                                                )
                                            ); ?>
                                    </a> 
                                </div>
                            </figure>

                            <div class="related_title py-2">
                                <?php the_title(); ?>  
                            </div>
                        </div>
                        <?php endif; ?>
                <?php 

                    endwhile; // end while
                        wp_reset_postdata();
                    endif;

                echo '</div>';    
            }    
            ?> 
    </main><!-- .site-main -->
<?php get_footer(); ?>